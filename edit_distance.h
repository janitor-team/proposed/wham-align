/*
 * nw.h for program nw.
 * TODO : integrate gap penalty ( currently can have hard coded )
 * return score/pos in int or just Int.
 */

#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

//#define EDIT_DEBUG 1
//#ifdef EXE
#define MAX 100
//#endif

typedef struct {
  int gap_penalty;
  int mm_penalty;
  int match_reward;
  int gap_ext_penalty;
} score_mat;

// all 1 means we are looking for edit distance
static score_mat obj_score_mat = { 1, 1, 1, 1 };

typedef struct {
  int score; // edit_distance
  int pos; // position of db_seq at which alignment found. this happens when we remove leading '-' from query
  int qgap; // number of gap in query (string2)
  int dgap; // number of gap in database ( string1)
  int mm; // number of mismatch in alignment
  int n_iden;
  int align_len;
} scoreinfo;

/*
 * parameter -
 * q_seq
 * db_seq
 * q_len
 * db_len
 * query_align
 * db_align
 * prm ( print matrix)
 */
scoreinfo edit_distance(char *, char *, int, int, char *, char *, int);

scoreinfo edit_distance_align(int **, char **, char *, char *, int, int, char *,
    char *, int);

/*typedef struct{
 char al_str1[MAX];
 char al_str2[MAX];
 scoreinfo score;
 int pos;
 }align_result;
 */

void matrix_init(int **, char **, int, int, int);
void print_al(char *, char *);
void print_matrix(int ** const, char *, char *, int, int);
void print_traceback(char ** const, char *, char *, int, int);
int min(int, int, int, char *);
void initialize_str(char *, int);
void reverse_str(char *, int);
void print_score(scoreinfo s);
void edit_distance_init();

