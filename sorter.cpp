/**
 *    WHAM - high-throughput sequence aligner
 *    Copyright (C) 2011  WHAM Group, University of Wisconsin
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*	$Id: main.cpp 135 2012-03-23 07:47:27Z yinan $ */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <pthread.h>
#ifndef WIN32
#include <sys/time.h>
#endif
#include <unistd.h>
#include <fstream>

#include "error.h"
#include "aligner.h"
#include "rdtsc.h"
#include "util.h"

using namespace std;

char pgversion[] = "0.1.3";
char * pgcommand;

int main(int argc, char* argv[]) {
  char basepath[256] = "";
  char newpath[256] = "";

  srand((unsigned int) time(NULL));

  strcpy(basepath, argv[1]);
  strcpy(newpath, argv[2]);

  if (basepath[0] == '\0') {
    elog(ERROR, "specify the base name.\n");
    printf("See usage message by specifying -h/--help.\n");
    exit(1);
  }

  if (newpath[0] == '\0') {
    elog(ERROR, "specify the new name.\n");
    printf("See usage message by specifying -h/--help.\n");
    exit(1);
  }

  elog(INFO, "loading WHAM indexes...\n");

  /* load index header */
  Aligner * aligner = new Aligner(basepath);

  aligner->printInfo();

  /* load hash tables */
  int ret = aligner->loadHashtables(basepath);
  if (ret != SUCCESS) {
    elog(ERROR, "failed to load indexes.\n");
  }

  aligner->sortList();
  /*
   elog(INFO, "saving indexes...\n");
   ret = aligner->saveHead(newpath);
   if (ret != SUCCESS) {
   elog(ERROR, "failed to save index head.\n");
   return ret;
   }
   ret = aligner->saveIndex(newpath, 0);
   if (ret != SUCCESS) {
   elog(ERROR, "failed to save index.\n");
   return ret;
   }
   */
  return SUCCESS;
}

