#ifndef _LIB_H_
#define _LIB_H_

/**
 *    WHAM - high-throughput sequence aligner
 *    Copyright (C) 2011  WHAM Group, University of Wisconsin
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*	$Id: lib.h 157 2012-07-25 05:58:09Z yinan $ */

#ifndef WIN32
typedef unsigned long long int64;
#else
typedef unsigned __int64 int64;
#endif

typedef unsigned int uint32;

#ifndef WIN32
#define RAND()		(rand())
#else
#define RAND()		((rand() << 15) | rand())
#endif

#define CACHE_LINE_SIZE 128

#define MAX_RESULT_POOL 16

#endif
